#include "kernel/yosys.h"
#include <algorithm>
#include <ffi.rs.h>

#include <iostream>
#include <iterator>
#include <string>
#include <map>
#include <optional>
#include <fstream>

#ifndef IS_SPADE
#define IS_SPADE 0
#endif

USING_YOSYS_NAMESPACE


struct FlamegraphPass : public Pass {
    FlamegraphPass() : Pass("dump_flamegraph", "") {
        /// Mapping of cell type to the port which is the output. This needs
        /// to be filled out manually because I can't find a way to automate it. For
        /// the ECP5, the mappings can be found here
        /// https://github.com/YosysHQ/nextpnr/blob/master/ecp5/cells.cc
        cell_outputs["\\TRELLIS_FF"] = "\\Q";
        cell_outputs["\\LUT4"] = "\\Z";
        cell_outputs["\\DCA"] = "\\Z";
        cell_outputs["\\TRELLIS_IO"] = "\\O";
        cell_outputs["\\TRELLIS_ECLKBUF"] = "\\ECLKO";
        // TRELLIS_COMB, TRELLIS_RAMW, CCU2C, IOLOGIC, SIOLOGIC have more than
        // one output, I'll have to think about how to handle it

        // https://github.com/YosysHQ/nextpnr/blob/master/ice40/cells.cc
        cell_outputs["\\SB_LUT4"] = "\\O";

        // cell_outputs["\\SB_RAM40_4K"] = "\\RDATA_0";

        // Generic
        cell_outputs["$_ANDNOT_"] = "\\Y";
        cell_outputs["$_AND_"] = "\\Y";
        cell_outputs["$_MUX_"] = "\\Y";
        cell_outputs["$_NAND_"] = "\\Y";
        cell_outputs["$_NOR_"] = "\\Y";
        cell_outputs["$_NOT_"] = "\\Y";
        cell_outputs["$_ORNOT_"] = "\\Y";
        cell_outputs["$_OR_"] = "\\Y";

        // sky130 sc hd
        // https://antmicro-skywater-pdk-docs.readthedocs.io/en/test-submodules-in-rtd/contents/libraries/sky130_fd_sc_hd/README.html
        cell_outputs["\\sky130_fd_sc_hd__dfxtp_2"] = "\\Q";
        cell_outputs["\\sky130_fd_sc_hd__a211o_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a21bo_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a21boi_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__a311o_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a31o_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a31oi_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__and2_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__and2b_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__and3_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__and3b_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__buf_1"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__buf_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__conb_1"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__inv_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__mux2_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__mux4_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__nand2_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__nand3_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__nand3b_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__nor2_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__o21a_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__o21ai_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__o21ba_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__o2bb2a_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__o311a_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__o31a_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__o32a_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__or2_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__or2b_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__or3b_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a21o_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a21oi_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__a221o_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a22oi_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__a32oi_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__o211a_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__o22a_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a211oi_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__a2bb2o_2"] = "\\X";
        cell_outputs["\\sky130_fd_sc_hd__a311oi_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__o211ai_2"] = "\\Y";
        cell_outputs["\\sky130_fd_sc_hd__o221a_2"] = "\\X";
    }

    virtual void execute(std::vector<std::string> args, RTLIL::Design *design) {
        // auto comp_state = read_state("build/state.ron");

#if (IS_SPADE == 1)
        if(args.size() != 4) {
            perror("dump_flamegraph takes 3 positional arguments\n");
            perror("   dump_flamegraph <top module> <output_dir> <compiler_state>\n");
            return;
        }
        std::string top = args[1];
        std::string output_dir = args[2];
        std::string compiler_state = args[3];

        log_warning("**USING TOP MODULE %s** and dumping flamegraph in %s\n", top.c_str(), output_dir.c_str());

        auto rcollection = new_resource_collection(compiler_state);
#else
        if(args.size() != 3) {
            perror("dump_flamegraph takes 2 positional arguments\n");
            perror("   dump_flamegraph <top module> <output_dir>\n");
            return;
        }
        std::string top = args[1];
        std::string output_dir = args[2];

        log_warning("**USING TOP MODULE %s** and dumping flamegraph in %s\n", top.c_str(), output_dir.c_str());

        auto rcollection = new_resource_collection();
#endif
        // Mapping of cell type -> (wire name -> count)
        std::map<std::string, std::optional<std::map<std::string, int>>> mapping;

        // Collect all the cells
        for (auto mod : design->modules()) {
            if (mod->name.str() == top) {
                for(auto cell : mod->cells()) {
                    std::string outport;

                    // If we have a manually specified output port, use it
                    if (cell_outputs.count(cell->type.str())) {
                        outport = cell_outputs[cell->type.str()];
                    }
                    else {
                        // If not, Check if there is a single output port and use that
                        std::vector<std::string> port_names;
                        for(auto&& [conn, _] : cell->connections()) {
                            if(cell->output(conn)) {
                                port_names.push_back(conn.str());
                            }
                        }

                        if(port_names.size() == 1) {
                            outport = port_names[0];
                        }
                        else {
                            // If there is more than one output port, things are ambiguous
                            std::vector<rust::String> output_names_rust;
                            std::transform(
                                port_names.begin(),
                                port_names.end(),
                                std::back_inserter(output_names_rust),
                                [](auto&& s) {return rust::String(s);}
                            );
                            rust::Slice<const rust::String> output_names_slice{
                                output_names_rust.data(),
                                output_names_rust.size()
                            };
                            rcollection->mark_unknown_output(cell->type.str(), output_names_slice);
                        }
                    }

                    try {
                        auto port = cell->getPort(outport);

                        if(port.is_chunk()) {
                            auto chunk = port.as_chunk();
                            auto wire = chunk.wire;

                            rcollection->increment_count(cell->type.str(), wire->name.str(), chunk.width);
                        }
                        else {
                            rcollection->add_weirdness(outport + " of" + cell->name.str() + " was not chunk");
                        }
                    }
                    catch (const std::exception& e) {
                        log_warning("Unhandled exception %s\n", e.what());
                        log_warning(
                            "Cell %s does not have port %s\n",
                            cell->name.c_str(),
                            outport.c_str()
                        );
                    }
                }
            }
        }

        // rcollection->print_flamegraph_data();
        rcollection->write_flamegraphs(output_dir);
    }

    std::map<std::string, std::string> cell_outputs;

} MyPass;

