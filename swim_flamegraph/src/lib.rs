use camino::Utf8Path;
use color_eyre::{eyre::Context, Result};
use spade::compiler_state::CompilerState;
use lazy_static::lazy_static;
use regex::Regex;
use yosys_flamegraph::Translator;

pub mod ffi;

struct SpadeVar {
    pub is_abc: bool,
    pub spade: String,
    pub rest: String,
}

enum SplitResult {
    Spade(SpadeVar),
    Unsplit(String)
}

impl SplitResult {
    pub fn into_strings(&self, compiler_state: &CompilerState) -> Vec<String> {
        match self {
            SplitResult::Spade(inner) => {
                let spade_name = compiler_state
                    .demangle_string(&inner.spade)
                    .unwrap_or_else(|| inner.spade.to_string());

                let mut demangled = if inner.is_abc {
                    "(abc)"
                }
                else {
                    ""
                }.to_string();
                // Add the original name to keep a unique ID
                demangled += &inner.spade;
                demangled += &spade_name;


                let mut result = vec![demangled];
                if !inner.rest.is_empty() {
                    result.push(inner.rest.clone())
                }
                result
            }
            SplitResult::Unsplit(inner) => vec![inner.clone()],
        }
    }
}

/// Yosys emits wire names like {name}_{resource_path}. Name is our spade
/// name which we want to translate, and the resource path is something we
/// don't really care about, so we'll split them in two. That way we visualise
/// the whole spade object as one, while still showing the split as another layer
fn split_spade_yosys_name(full_name: &str) -> SplitResult {
    lazy_static! {
        static ref NAME_RE: Regex =
            Regex::new(r#"\\?(?P<abc_prelude>\$abc\$\d+\$)?(?P<spade_name>(_e_\d+|\w+_n\d+_?))(_i)?(?P<rest>.*)"#).unwrap();
    }

    match NAME_RE.captures(full_name) {
        Some(cap) => {
            let spade_name = cap
                .name("spade_name")
                .map(|m| m.as_str().to_string())
                .unwrap_or(String::new());

            let rest = cap
                .name("rest")
                .map(|m| m.as_str().to_string())
                .unwrap_or(String::new());

            SplitResult::Spade(SpadeVar {
                spade: spade_name,
                rest,
                is_abc: cap.name("abc_prelude").is_some()
            })
        }
        None => SplitResult::Unsplit(full_name.to_string())
    }
}

pub struct SpadeTranslator {
    compiler_state: CompilerState,
}

impl SpadeTranslator {
    pub fn new(path: &Utf8Path) -> Result<Self> {
        let state_str = std::fs::read_to_string(path)
            .with_context(|| format!("Failed to read compiler state from {path}"))?;

        let ron = ron::Options::default().without_recursion_limit();
        let compiler_state = ron.from_str::<CompilerState>(&state_str)
            .with_context(|| format!("Failed to decode compiler state from {path}"))?;

        Ok(Self { compiler_state })
    }
}

impl Translator for SpadeTranslator {
    fn translate(&self, name: &str) -> Vec<String> {
        println!("Translating full name {name}");
        split_spade_yosys_name(name)
            .into_strings(&self.compiler_state)
    }
}


