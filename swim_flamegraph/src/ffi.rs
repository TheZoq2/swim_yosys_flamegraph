use crate::{SpadeTranslator};
use camino::Utf8PathBuf;
use color_eyre::Result;

pub struct ResourceCollection(yosys_flamegraph::ResourceCollection<SpadeTranslator>);

impl ResourceCollection {
        fn increment_count(&mut self, cell: &str, name: &str, amount: u64) -> Result<()> {
            self.0.increment_count(cell, name, amount)
        }
        fn mark_unknown_output(&mut self, cell: &str, ports: &[String]) -> Result<()> {
            self.0.mark_unknown_output(cell, ports)
        }
        fn write_flamegraphs(&self, output_dir: &str) -> Result<()> {
            self.0.write_flamegraphs(output_dir)
        }
        fn add_weirdness(&mut self, message: &str) {
            self.0.add_weirdness(message)
        }
}

#[cxx::bridge]
mod ffi {
    extern "Rust" {
        type ResourceCollection;
        fn new_resource_collection(state_file: String) -> Result<Box<ResourceCollection>>;

        fn increment_count(&mut self, cell: &str, name: &str, amount: u64) -> Result<()>;
        fn mark_unknown_output(&mut self, cell: &str, ports: &[String]) -> Result<()>;
        fn write_flamegraphs(&self, output_dir: &str) -> Result<()>;

        fn add_weirdness(&mut self, description: &str);
    }
}

pub fn new_resource_collection(state_file: String) -> Result<Box<ResourceCollection>> {
    Ok(Box::new(ResourceCollection(
        yosys_flamegraph::ResourceCollection {
            cell_data: std::collections::hash_map::HashMap::new(),
            translator: SpadeTranslator::new(&Utf8PathBuf::from(state_file))?,
            weirdness: vec![]
        },
    )))
}
