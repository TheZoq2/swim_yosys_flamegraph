# Yosys flamegraph

![Screenshot of a flamegraph](./screenshot.png)

Visualise resource usage of synthesised design using [flamegraphs](https://www.brendangregg.com/flamegraphs.html)

## Current status

The plugin is currently experimental, but might be useful to some. There is a
big limitation at the moment which means that only cells with a single output can
 be visualised correctly, it interprets the "user" of a cell as the wire which its output is fed to.

There is no automatic inference of output ports, so the plugin must be told about
which cells and ports you want to use. Adding more cells is done by extending
the `cell_outputs` map in the plugin constructors. Merge requests for more cells
are very appreciated :)

## Standalone usage

### Compilation
To use this plugin in a normal yosys flow, you must first compile the binary. This requires
a rust installation which is easiest to get via https://rustup.rs/

To build the plugin first build the rust lib
```
cargo build --release
```
then build the full yosys plugin
```
make
```

### Usage

The plugin reports the result of synthesis, so before using it, run your normal synthesis command(s), like `synth_ice40` or `synth_verilog`.

In the same yosys session you can then use the plugin by first loading it with
```
yosys> plugin -i build/dump_flamegraph.so
```
the Makefile puts the library in the `build` directory by default

Then, run
```
dump_flamegraph <top_module> <output_dir>
```
to build the flamegraphs. It will give you path to a HTML report. The
`output_dir` is where the resulting HTML and SVG files will be emitted.
Finally, open the resulting `html` file in your favourite browser.

> Hint: The name of the top module is not always what you expect, it can
> sometimes contain backslashes for example. If you get no output, makes sure
> that the top you specified matches the top name in yosys as shown by, for
> example, `hiearchy`.


## Use as a swim plugin

Add the plugin to your `swim.toml`

```
[plugins]
flamegraph = {git = "https://gitlab.com/TheZoq2/yosys_flamegraph.git", branch = "main"}
```
