use inferno::flamegraph::{
    color::{BackgroundColor, Color},
    Options,
};

pub fn flamegraph_options<'a>(title: &str) -> Options<'a> {
    let mut options = Options::default();
    options.title = title.to_string();
    options.bgcolors = Some(BackgroundColor::Flat(Color {
        r: 11,
        g: 21,
        b: 29,
    }));
    options.uicolor = Color{r: 255, g: 255, b: 255};
    options
}

pub fn css_style() -> String {
    r#"
        .flamegraph {
            width: 100%;
        }

        body {
            background-color: #0b151d;
            color: #fff;
        }

        h1 {
            font-family: monospace;
            font-size: 1.6em;
        }

        p {
            font-family: monospace;
        }
    "#
    .to_string()
}
