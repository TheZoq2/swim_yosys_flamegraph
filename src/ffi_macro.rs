#[macro_export]
macro_rules! build_ffi {
    () => {
        extern "Rust" {
            type ResourceCollection;
            fn new_resource_collection() -> Box<ResourceCollection>;

            fn increment_count(&mut self, cell: &str, name: &str, amount: u64) -> Result<()>;
            fn mark_unknown_output(&mut self, cell: &str) -> Result<()>;
            fn write_flamegraphs(&self, output_dir: &str) -> Result<()>;
        }
    }
}
