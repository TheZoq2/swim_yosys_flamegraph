use crate::DummyTranslator;

type ResourceCollection = crate::ResourceCollection<crate::DummyTranslator>;

#[cxx::bridge]
mod ffi {
    extern "Rust" {
        type ResourceCollection;
        fn new_resource_collection() -> Box<ResourceCollection>;

        fn increment_count(&mut self, cell: &str, name: &str, amount: u64) -> Result<()>;
        fn mark_unknown_output(&mut self, cell: &str, outputs: &[String]) -> Result<()>;
        fn write_flamegraphs(&self, output_dir: &str) -> Result<()>;

        fn add_weirdness(&mut self, output_dir: &str);
    }
}


pub fn new_resource_collection() -> Box<ResourceCollection> {
    Box::new(ResourceCollection {
        cell_data: std::collections::hash_map::HashMap::new(),
        translator: DummyTranslator {},
        weirdness: vec![]
    })
}

