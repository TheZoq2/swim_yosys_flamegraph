use camino::Utf8Path;
use color_eyre::{eyre::Context, Result};
use spade::compiler_state::CompilerState;
use lazy_static::lazy_static;

pub struct Translator {
    compiler_state: CompilerState,
}

impl Translator {
    pub fn new(path: &Utf8Path) -> Result<Self> {
        let state_str = std::fs::read_to_string(path)
            .with_context(|| format!("Failed to read compiler state from {path}"))?;
        let compiler_state = ron::from_str::<CompilerState>(&state_str)
            .with_context(|| format!("Failed to decode compiler state from {path}"))?;

        Ok(Self { compiler_state })
    }

    pub fn translate(&self, name: &str) -> String {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(\$abc\$\d+\$)?([A-z_0-9]+)(_i)?").unwrap();
        }

        let mut inner_name = if let Some(cap) = RE.captures(name) {
            cap[1]
        };

        let inner_name = if inner_name.ends_with("_i") {
            inner_name[..-2]
        }
        else {
            inner_name
        };

        self.compiler_state
            .demangle_string(inner_name)
            .unwrap_or_else(|| name.to_string())
    }
}
