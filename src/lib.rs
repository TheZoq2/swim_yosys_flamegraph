use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt::Write;
use std::fs::File;

use color_eyre::eyre::anyhow;
use color_eyre::eyre::bail;
use color_eyre::eyre::Context;
use color_eyre::eyre::Result;
use html_builder::Html5;
use inferno::flamegraph;
use itertools::Itertools;

use crate::theme::css_style;

#[cfg(feature = "ffi")]
mod ffi;
mod ffi_macro;
mod theme;

pub trait Translator {
    fn translate(&self, name: &str) -> Vec<String>;
}

pub struct DummyTranslator {}
impl Translator for DummyTranslator {
    fn translate(&self, name: &str) -> Vec<String> {
        vec![name.to_string()]
    }
}

pub struct ResourceCollection<T: Translator> {
    pub cell_data: HashMap<String, CellData<HashMap<String, u64>>>,
    pub translator: T,
    pub weirdness: Vec<String>,
}

impl<T: Translator> ResourceCollection<T> {
    pub fn increment_count(&mut self, cell: &str, name: &str, amount: u64) -> Result<()> {
        *self
            .cell_data
            .entry(cell.to_string())
            .or_insert(CellData::Valid(HashMap::new()))
            .assume_counts()
            .with_context(|| anyhow!("When adding adding count to  {cell}"))?
            .entry(name.to_string())
            .or_insert(0) += amount;

        Ok(())
    }

    pub fn mark_unknown_output(&mut self, cell: &str, ports: &[String]) -> Result<()> {
        match self.cell_data.get(&cell.to_owned()) {
            Some(CellData::UnknownOutput(_)) => Ok(()),
            Some(CellData::Valid(_)) => {
                bail!("Marking {cell} as unknown, but it already has counts")
            }
            None => {
                self.cell_data
                    .insert(cell.to_owned(), CellData::UnknownOutput(ports.into()));
                Ok(())
            }
        }
    }

    pub fn add_weirdness(&mut self, message: &str) {
        self.weirdness.push(message.to_string())
    }

    pub fn write_flamegraphs(&self, output_dir: &str) -> Result<()> {
        let mut all_data = self.generate_flamegraph_data().into_iter().collect_vec();
        all_data.sort_by(|(lcell, ldata), (rcell, rdata)| match (ldata, rdata) {
            (CellData::Valid(_), CellData::UnknownOutput(_)) => Ordering::Less,
            (CellData::UnknownOutput(_), CellData::Valid(_)) => Ordering::Greater,
            (_, _) => lcell.cmp(rcell),
        });

        let mut buf = html_builder::Buffer::new();

        buf.doctype();
        let mut html = buf.html();
        let mut head = html.head();
        head.meta().attr("charset='utf-8'");
        head.style().write_str(&css_style())?;
        let mut body = html.body();

        if !self.weirdness.is_empty() {
            println!("**NOTE** Found weirdness");
            let mut p_tag = body.p().attr(r#"class=weirdness_counter"#);
            writeln!(p_tag, r#"NOTE: Found some weirdness"#)?;
        }

        for (cell, data) in all_data {
            let mut block = body.div();

            match data {
                CellData::Valid(lines) => {
                    let filename = format!("{output_dir}/{cell}.svg").replace("\\", "");
                    let output = File::create(&filename)
                        .with_context(|| format!("Failed to open {filename}"))?;

                    flamegraph::from_lines(
                        &mut theme::flamegraph_options(&cell),
                        lines.iter().map(String::as_str),
                        output,
                    )
                    .with_context(|| format!("Failed to generate flamegaph for {cell}",))?;
                    block
                        .object()
                        .attr(&format!(r#"data="{filename}""#))
                        .attr(r#"type="image/svg+xml""#)
                        .attr(r#"class="flamegraph""#);
                }
                CellData::UnknownOutput(ports) => {
                    writeln!(block.h1(), "{cell}")?;
                    writeln!(block.p(), "The output port of this cell is unknown")?;
                    writeln!(block.p(), "Available ports are {}", ports.join(", "))?;
                }
            }
        }


        let mut weirdness_block = body.div();
        // let mut weirdness_heading = weirdness_block.h1().attr(r#"id="weirdness"#);
        // writeln!(weirdness_heading, "Weirdness")?;
        if !self.weirdness.is_empty() {
            for weirdness in &self.weirdness {
                writeln!(weirdness_block.p(), "{weirdness}")?;
            }
        }


        let full_html = buf.finish();
        let html_file = format!("{output_dir}/flamegraphs.html");
        std::fs::write(&html_file, full_html)
            .with_context(|| format!("Faield to write flamegraph html to"))?;

        println!("Wrote flamegraph report to {html_file}");
        Ok(())
    }

    pub fn generate_flamegraph_data(&self) -> HashMap<String, CellData<Vec<String>>> {
        self.cell_data
            .iter()
            .map(|(cell_name, data)| {
                let lines = match data {
                    CellData::Valid(counts) => {
                        let counts = counts
                            .iter()
                            .map(|(wire_name, count)| {
                                let stack = wire_name
                                    .split(".")
                                    .map(|mangled| self.translator.translate(&mangled))
                                    .flatten()
                                    .join(";");

                                format!("{stack}:{count} {count}")
                            })
                            .collect();
                        CellData::Valid(counts)
                    }
                    CellData::UnknownOutput(d) => CellData::UnknownOutput(d.clone()),
                };
                (cell_name.clone(), lines)
            })
            .collect()
    }
}

pub enum CellData<T> {
    Valid(T),
    /// The tool does not know what ports of this cell are outputs.
    UnknownOutput(Vec<String>),
}

impl<T> CellData<T> {
    pub fn assume_counts(&mut self) -> Result<&mut T> {
        match self {
            CellData::Valid(c) => Ok(c),
            CellData::UnknownOutput(_) => bail!("Marked as unknown output"),
        }
    }
}
