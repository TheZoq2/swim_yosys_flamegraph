# Requires the caller to cargo build this directory first

ifeq ($(BUILD_DIR),)
	BUILD_DIR = "build"
endif

ifeq ($(IS_SPADE),1)
	IS_SPADE_FLAG := -DIS_SPADE=1
	FLAMEGRAPH_INCLUDE_DIR=swim_flamegraph/target/cxxbridge/swim_flamegraph/src/
	FLAMEGRAPH_LIB_DIR=swim_flamegraph/target/release
	FLAMEGRAPH_A=${FLAMEGRAPH_LIB_DIR}/libswim_flamegraph.a
	LIB_NAME=swim_flamegraph
else
	FLAMEGRAPH_INCLUDE_DIR=target/cxxbridge/yosys_flamegraph/src/
	FLAMEGRAPH_LIB_DIR=target/release
	FLAMEGRAPH_A=${FLAMEGRAPH_LIB_DIR}/libyosys_flamegraph.a
	LIB_NAME=yosys_flamegraph
endif


${BUILD_DIR}/dump_flamegraph.so: dump_flamegraph.cpp Makefile ${FLAMEGRAPH_A}
	mkdir -p $(@D)
	yosys-config \
		--exec \
		--cxx \
		--cxxflags \
		--ldflags \
		-o $@ \
		-shared \
		--ldlibs \
		dump_flamegraph.cpp \
		-Wno-deprecated-declarations \
		-I${FLAMEGRAPH_INCLUDE_DIR} \
		-l${LIB_NAME} \
		-L${FLAMEGRAPH_LIB_DIR} \
		-std=c++17 \
		$(IS_SPADE_FLAG)

